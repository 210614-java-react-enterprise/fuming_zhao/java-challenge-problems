package com.revature.eval.java.core;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoField;
import java.time.temporal.Temporal;
import java.util.*;

public class EvaluationService {



	/**
	 * 1. Without using the StringBuilder or StringBuffer class, write a method that
	 * reverses a String. Example: reverse("example"); -> "elpmaxe"
	 * 
	 * @param string
	 * @return
	 */
	public String reverse(String string) {
		char[] reversed = new char[string.length()];
		for (int i = 0; i < string.length(); i++ ){
			reversed[string.length()-i-1] = string.charAt(i);
		}

		return new String(reversed);
	}

	/**
	 * 2. Convert a phrase to its acronym. Techies love their TLA (Three Letter
	 * Acronyms)! Help generate some jargon by writing a program that converts a
	 * long name like Portable Network Graphics to its acronym (PNG).
	 * 
	 * @param phrase
	 * @return
	 */
	public String acronym(String phrase) {
		// TODO Write an implementation for this method declaration
		String result = "";
		boolean boundary = false;
		result += phrase.charAt(0);
		for(int i = 0; i < phrase.length(); i++){
			if(!Character.isLetter(phrase.charAt(i))){
				boundary = true;
			}
			else if (boundary == true){
				result += phrase.charAt(i);
				boundary = false;
			}
		}
		result = result.toUpperCase();
//		String[] s = phrase.split(" ");
//		String result = "";
//
//		for (String word:s){
//			result += word.charAt(0);
//
//		}
		return result;
	}

	/**
	 * 3. Determine if a triangle is equilateral, isosceles, or scalene. An
	 * equilateral triangle has all three sides the same length. An isosceles
	 * triangle has at least two sides the same length. (It is sometimes specified
	 * as having exactly two sides the same length, but for the purposes of this
	 * exercise we'll say at least two.) A scalene triangle has all sides of
	 * different lengths.
	 *
	 */
	static class Triangle {
		private double sideOne;
		private double sideTwo;
		private double sideThree;

		public Triangle() {
			super();
		}

		public Triangle(double sideOne, double sideTwo, double sideThree) {
			this();
			this.sideOne = sideOne;
			this.sideTwo = sideTwo;
			this.sideThree = sideThree;
		}

		public double getSideOne() {
			return sideOne;
		}

		public void setSideOne(double sideOne) {
			this.sideOne = sideOne;
		}

		public double getSideTwo() {
			return sideTwo;
		}

		public void setSideTwo(double sideTwo) {
			this.sideTwo = sideTwo;
		}

		public double getSideThree() {
			return sideThree;
		}

		public void setSideThree(double sideThree) {
			this.sideThree = sideThree;
		}

		public boolean isEquilateral() {
			// TODO Write an implementation for this method declaration
			if (getSideOne() == getSideTwo() && getSideTwo() == getSideThree()){
				return true;
			}
			else{
				return false;
			}
		}

		public boolean isIsosceles() {
			// TODO Write an implementation for this method declaration
			if (getSideOne() == getSideTwo() || getSideTwo() == getSideThree() || getSideOne() == getSideThree()){
				return true;
			}
			else{
				return false;
			}
		}

		public boolean isScalene() {
			// TODO Write an implementation for this method declaration
			if(getSideOne() != getSideTwo() && getSideTwo() != getSideThree()){
				return true;
			}
			else{
				return false;
			}

		}

	}

	/**
	 * 4. Given a word, compute the scrabble score for that word.
	 * 
	 * --Letter Values-- Letter Value A, E, I, O, U, L, N, R, S, T = 1; D, G = 2; B,
	 * C, M, P = 3; F, H, V, W, Y = 4; K = 5; J, X = 8; Q, Z = 10; Examples
	 * "cabbage" should be scored as worth 14 points:
	 * 
	 * 3 points for C, 1 point for A, twice 3 points for B, twice 2 points for G, 1
	 * point for E And to total:
	 * 
	 * 3 + 2*1 + 2*3 + 2 + 1 = 3 + 2 + 6 + 3 = 5 + 9 = 14
	 * 
	 * @param string
	 * @return
	 */
	public int getScrabbleScore(String string) {
		// TODO Write an implementation for this method declaration
		int score = 0;
		Set<Character> setValue1 = new HashSet<Character>(Arrays.asList('A', 'E', 'I', 'O', 'U', 'L', 'N', 'R', 'S', 'T'));
		Set<Character> setValue2 = new HashSet<Character>(Arrays.asList('D', 'G'));
		Set<Character> setValue3 = new HashSet<Character>(Arrays.asList('B', 'C', 'M', 'P'));
		Set<Character> setValue4 = new HashSet<Character>(Arrays.asList('F', 'H', 'V', 'W', 'Y'));
		Set<Character> setValue5 = new HashSet<Character>(Arrays.asList('K'));
		Set<Character> setValue8 = new HashSet<Character>(Arrays.asList('J', 'X'));
		Set<Character> setValue10 = new HashSet<Character>(Arrays.asList('Q', 'Z'));
		for(char c: string.toUpperCase().toCharArray()){
			if(setValue1.contains(c)){
				score += 1;
			} else if(setValue2.contains(c)){
				score += 2;
			} else if(setValue3.contains(c)){
				score += 3;
			} else if(setValue4.contains(c)){
				score += 4;
			} else if(setValue5.contains(c)){
				score += 5;
			} else if(setValue8.contains(c)){
				score += 8;
			} else if(setValue10.contains(c)){
				score += 10;
			}
		}
		return score;
	}

	/**
	 * 5. Clean up user-entered phone numbers so that they can be sent SMS messages.
	 * 
	 * The North American Numbering Plan (NANP) is a telephone numbering system used
	 * by many countries in North America like the United States, Canada or Bermuda.
	 * All NANP-countries share the same international country code: 1.
	 * 
	 * NANP numbers are ten-digit numbers consisting of a three-digit Numbering Plan
	 * Area code, commonly known as area code, followed by a seven-digit local
	 * number. The first three digits of the local number represent the exchange
	 * code, followed by the unique four-digit number which is the subscriber
	 * number.
	 * 
	 * The format is usually represented as
	 * 
	 * 1 (NXX)-NXX-XXXX where N is any digit from 2 through 9 and X is any digit
	 * from 0 through 9.
	 * 
	 * Your task is to clean up differently formatted telephone numbers by removing
	 * punctuation and the country code (1) if present.
	 * 
	 * For example, the inputs
	 * 
	 * +1 (613)-995-0253 613-995-0253 1 613 995 0253 613.995.0253 should all produce
	 * the output
	 * 
	 * 6139950253
	 * 
	 * Note: As this exercise only deals with telephone numbers used in
	 * NANP-countries, only 1 is considered a valid country code.
	 */
	public String cleanPhoneNumber(String string) {
		// TODO Write an implementation for this method declaration
		String number = "";
		for(char c: string.toUpperCase().toCharArray()){
			if(Character.isDigit(c)){
				number += c;
			}
		}
		String result;
		if(number.charAt(0) == '1'){
			number = number.substring(1);
		}
		if (number.length() != 10){
			throw new IllegalArgumentException();
		}
		return number;
	}

	/**
	 * 6. Given a phrase, count the occurrences of each word in that phrase.
	 * 
	 * For example for the input "olly olly in come free" olly: 2 in: 1 come: 1
	 * free: 1
	 * 
	 * @param string
	 * @return
	 */
	public Map<String, Integer> wordCount(String string) {
		// TODO Write an implementation for this method declaration
		string = string.replace('\n',' ').replace('.', ' ').replace(',', ' ');
		String[] words = string.split("\\s+"); //one or more while spaces

		HashMap<String, Integer> count = new HashMap<String, Integer>();

		for(String word: words) {
			if (count.containsKey(word)) {
				count.put(word, count.get(word) + 1);
			} else {
				count.put(word, 1);
			}
		}
		return count;
	}

	/**
	 * 7. Implement a binary search algorithm.
	 * 
	 * Searching a sorted collection is a common task. A dictionary is a sorted list
	 * of word definitions. Given a word, one can find its definition. A telephone
	 * book is a sorted list of people's names, addresses, and telephone numbers.
	 * Knowing someone's name allows one to quickly find their telephone number and
	 * address.
	 * 
	 * If the list to be searched contains more than a few items (a dozen, say) a
	 * binary search will require far fewer comparisons than a linear search, but it
	 * imposes the requirement that the list be sorted.
	 * 
	 * In computer science, a binary search or half-interval search algorithm finds
	 * the position of a specified input value (the search "key") within an array
	 * sorted by key value.
	 * 
	 * In each step, the algorithm compares the search key value with the key value
	 * of the middle element of the array.
	 * 
	 * If the keys match, then a matching element has been found and its index, or
	 * position, is returned.
	 * 
	 * Otherwise, if the search key is less than the middle element's key, then the
	 * algorithm repeats its action on the sub-array to the left of the middle
	 * element or, if the search key is greater, on the sub-array to the right.
	 * 
	 * If the remaining array to be searched is empty, then the key cannot be found
	 * in the array and a special "not found" indication is returned.
	 * 
	 * A binary search halves the number of items to check with each iteration, so
	 * locating an item (or determining its absence) takes logarithmic time. A
	 * binary search is a dichotomic divide and conquer search algorithm.
	 * 
	 */
	static class BinarySearch<T extends Comparable<T>> { //added "extends Comparable<T>"
		private List<T> sortedList;

		public int indexOf(T t) {
			// TODO Write an implementation for this method declaration
			int begin = 0;
			int end = sortedList.size() - 1;
			int mid = 0;

			while (begin <= end) {
				mid = (begin + end ) / 2;
				if (sortedList.get(mid).compareTo(t) < 0){
					begin = mid + 1;
				}else if(sortedList.get(mid).compareTo(t) > 0){
					end = mid -1;

				}else{
					return mid;
				}
			}
			return -1;
		}

		public BinarySearch(List<T> sortedList) {
			super();
			this.sortedList = sortedList;
		}

		public List<T> getSortedList() {
			return sortedList;
		}

		public void setSortedList(List<T> sortedList) {
			this.sortedList = sortedList;
		}

	}

	/**
	 * 8. Implement a program that translates from English to Pig Latin.
	 * 
	 * Pig Latin is a made-up children's language that's intended to be confusing.
	 * It obeys a few simple rules (below), but when it's spoken quickly it's really
	 * difficult for non-children (and non-native speakers) to understand.
	 * 
	 * Rule 1: If a word begins with a vowel sound, add an "ay" sound to the end of
	 * the word. Rule 2: If a word begins with a consonant sound, move it to the end
	 * of the word, and then add an "ay" sound to the end of the word. There are a
	 * few more rules for edge cases, and there are regional variants too.
	 * 
	 * See http://en.wikipedia.org/wiki/Pig_latin for more details.
	 * 
	 * @param string
	 * @return
	 */
	public String toPigLatin(String string) {
		// TODO Write an implementation for this method declaration
		Set<Character> vowels = new HashSet<Character>(Arrays.asList('A', 'E', 'I', 'O', 'U', 'a', 'e', 'i', 'o', 'u'));
		String[] words = string.split("\\s+"); //one or more while spaces
		for(String word:words){
			int i = 0;
			char c = word.charAt(i);
			while(!vowels.contains(c)){ // find the index of first vowel
				i+=1;
				c = word.charAt(i);
			}
			String replaced = word.substring(i) + word.substring(0,i) + "ay";
			if(word.startsWith("qu")){ //handle edge case
				replaced = word.substring(2) + "qu" + "ay";
			}
			string = string.replace(word, replaced);
		}
		return string;
//		int i = 0;
//		char c = string.charAt(i);
//		while(!vowels.contains(c)){
//			i+=1;
//			c = string.charAt(i);
//		}
//		String f = string.substring(i) + string.substring(0,i) + "ay";
//		return f;
	}

	/**
	 * 9. An Armstrong number is a number that is the sum of its own digits each
	 * raised to the power of the number of digits.
	 * 
	 * For example:
	 * 
	 * 9 is an Armstrong number, because 9 = 9^1 = 9 10 is not an Armstrong number,
	 * because 10 != 1^2 + 0^2 = 2 153 is an Armstrong number, because: 153 = 1^3 +
	 * 5^3 + 3^3 = 1 + 125 + 27 = 153 154 is not an Armstrong number, because: 154
	 * != 1^3 + 5^3 + 4^3 = 1 + 125 + 64 = 190 Write some code to determine whether
	 * a number is an Armstrong number.
	 * 
	 * @param input
	 * @return
	 */
	public boolean isArmstrongNumber(int input) {
		// TODO Write an implementation for this method declaration
		String number = String.valueOf(input);
		int sum = 0;
		for(char c: number.toCharArray()){
			sum += Math.pow(Character.getNumericValue(c), number.length());
		}
		if(input == sum){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * 10. Compute the prime factors of a given natural number.
	 * 
	 * A prime number is only evenly divisible by itself and 1.
	 * 
	 * Note that 1 is not a prime number.
	 * 
	 * @param l
	 * @return
	 */
	public List<Long> calculatePrimeFactorsOf(long l) {
		// TODO Write an implementation for this method declaration
		ArrayList<Long> factors = new ArrayList<Long>();
        long current = l;
        for(long i=2; i<=current; i++){
            while(current % i == 0){
            	factors.add(i);
                current = current/i;
            }
        }
		return factors;
	}

	/**
	 * 11. Create an implementation of the rotational cipher, also sometimes called
	 * the Caesar cipher.
	 * 
	 * The Caesar cipher is a simple shift cipher that relies on transposing all the
	 * letters in the alphabet using an integer key between 0 and 26. Using a key of
	 * 0 or 26 will always yield the same output due to modular arithmetic. The
	 * letter is shifted for as many values as the value of the key.
	 * 
	 * The general notation for rotational ciphers is ROT + <key>. The most commonly
	 * used rotational cipher is ROT13.
	 * 
	 * A ROT13 on the Latin alphabet would be as follows:
	 * 
	 * Plain: abcdefghijklmnopqrstuvwxyz Cipher: nopqrstuvwxyzabcdefghijklm It is
	 * stronger than the Atbash cipher because it has 27 possible keys, and 25
	 * usable keys.
	 * 
	 * Ciphertext is written out in the same formatting as the input including
	 * spaces and punctuation.
	 * 
	 * Examples: ROT5 omg gives trl ROT0 c gives c ROT26 Cool gives Cool ROT13 The
	 * quick brown fox jumps over the lazy dog. gives Gur dhvpx oebja sbk whzcf bire
	 * gur ynml qbt. ROT13 Gur dhvpx oebja sbk whzcf bire gur ynml qbt. gives The
	 * quick brown fox jumps over the lazy dog.
	 */
	static class RotationalCipher {
		private int key;

		public RotationalCipher(int key) {
			super();
			this.key = key;
		}

		public String rotate(String string) {
			// TODO Write an implementation for this method declaration
			StringBuilder result = new StringBuilder();
			for (char c : string.toCharArray()){
				if(Character.isLetter(c)){
					char letter_a;
					if(Character.isLowerCase(c)) {
						letter_a = 'a';
					}else {
						letter_a = 'A';
					}
					int old_in_ASCII = c - letter_a;
					int new_in_ASCII = (old_in_ASCII + key) % 26;
					int position_in_ASCII = letter_a + new_in_ASCII;
					char newC = (char)position_in_ASCII;
					result.append(newC);
				}else{
					result.append(c);
				}
			}
			return new String(result);
		}

	}

	/**
	 * 12. Given a number n, determine what the nth prime is.
	 * 
	 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see
	 * that the 6th prime is 13.
	 * 
	 * If your language provides methods in the standard library to deal with prime
	 * numbers, pretend they don't exist and implement them yourself.
	 * 
	 * @param i
	 * @return
	 */
	public int calculateNthPrime(int i) {
		// TODO Write an implementation for this method declaration
		if(i < 1){
			throw new IllegalArgumentException();
		}
		int count = 0;
		int n = i;
		int number = 1;
		while(count < n) {
			number = number + 1;
			int j;
			for(j=2; j<number; j++) {
				if (number % j == 0) {
					break; //it is not a prime number
				}
			}
			if(j == number){
				count += 1; //prime number is found
			}
		}
		return number;
	}

	/**
	 * 13 & 14. Create an implementation of the atbash cipher, an ancient encryption
	 * system created in the Middle East.
	 * 
	 * The Atbash cipher is a simple substitution cipher that relies on transposing
	 * all the letters in the alphabet such that the resulting alphabet is
	 * backwards. The first letter is replaced with the last letter, the second with
	 * the second-last, and so on.
	 * 
	 * An Atbash cipher for the Latin alphabet would be as follows:
	 * 
	 * Plain: abcdefghijklmnopqrstuvwxyz Cipher: zyxwvutsrqponmlkjihgfedcba It is a
	 * very weak cipher because it only has one possible key, and it is a simple
	 * monoalphabetic substitution cipher. However, this may not have been an issue
	 * in the cipher's time.
	 * 
	 * Ciphertext is written out in groups of fixed length, the traditional group
	 * size being 5 letters, and punctuation is excluded. This is to make it harder
	 * to guess things based on word boundaries.
	 * 
	 * Examples Encoding test gives gvhg Decoding gvhg gives test Decoding gsvjf
	 * rxpyi ldmul cqfnk hlevi gsvoz abwlt gives thequickbrownfoxjumpsoverthelazydog
	 *
	 */
	static class AtbashCipher {

		/**
		 * Question 13
		 * 
		 * @param string
		 * @return
		 */
		public static String encode(String string) {
			// TODO Write an implementation for this method declaration
			StringBuilder result = new StringBuilder();
			int count = 0;
			for (char c : string.toCharArray()){
				if(Character.isLetter(c)){
					int old_in_ASCII = Character.toLowerCase(c) - 'a';
					int new_in_ASCII = 25 - old_in_ASCII;
					int position_in_ASCII = 'a' + new_in_ASCII;
					char newC = (char)position_in_ASCII;
					if(count==5){
						result.append(' ');
						count = 0;
					}
					result.append(newC);
					count+=1;
				}else if (Character.isDigit(c)){

					if(count==5){
						result.append(' ');
						count = 0;
					}
					result.append(c);
					count+=1;
				}
			}
			return new String(result);
		}

		/**
		 * Question 14
		 * 
		 * @param string
		 * @return
		 */
		public static String decode(String string) {
			// TODO Write an implementation for this method declaration
			StringBuilder result = new StringBuilder();
			for (char c : string.toCharArray()){
				if(Character.isLetter(c)){
					int old_in_ASCII = Character.toLowerCase(c) - 'a';
					int new_in_ASCII = 25 - old_in_ASCII;
					int position_in_ASCII = 'a' + new_in_ASCII;
					char newC = (char)position_in_ASCII;
					result.append(newC);
				}else if (Character.isDigit(c)){
					result.append(c);
				}
			}
			return new String(result);
		}
	}

	/**
	 * 15. The ISBN-10 verification process is used to validate book identification
	 * numbers. These normally contain dashes and look like: 3-598-21508-8
	 * 
	 * ISBN The ISBN-10 format is 9 digits (0 to 9) plus one check character (either
	 * a digit or an X only). In the case the check character is an X, this
	 * represents the value '10'. These may be communicated with or without hyphens,
	 * and can be checked for their validity by the following formula:
	 * 
	 * (x1 * 10 + x2 * 9 + x3 * 8 + x4 * 7 + x5 * 6 + x6 * 5 + x7 * 4 + x8 * 3 + x9
	 * * 2 + x10 * 1) mod 11 == 0 If the result is 0, then it is a valid ISBN-10,
	 * otherwise it is invalid.
	 * 
	 * Example Let's take the ISBN-10 3-598-21508-8. We plug it in to the formula,
	 * and get:
	 * 
	 * (3 * 10 + 5 * 9 + 9 * 8 + 8 * 7 + 2 * 6 + 1 * 5 + 5 * 4 + 0 * 3 + 8 * 2 + 8 *
	 * 1) mod 11 == 0 Since the result is 0, this proves that our ISBN is valid.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isValidIsbn(String string) {
		// TODO Write an implementation for this method declaration
		int sum = 0;
		int number = 10;
		for(char c:string.toCharArray()){
			if(Character.isDigit(c)){
				sum += Character.getNumericValue(c) * number;
				number--;
			}else if(c == 'X'){
				sum += 10 * number;
				number--;
			}
		}

		if(number==0 && sum % 11 == 0){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * 16. Determine if a sentence is a pangram. A pangram (Greek: παν γράμμα, pan
	 * gramma, "every letter") is a sentence using every letter of the alphabet at
	 * least once. The best known English pangram is:
	 * 
	 * The quick brown fox jumps over the lazy dog.
	 * 
	 * The alphabet used consists of ASCII letters a to z, inclusive, and is case
	 * insensitive. Input will not contain non-ASCII symbols.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isPangram(String string) {
		// TODO Write an implementation for this method declaration
		string = string.toLowerCase();
		Set<Character> currentAlphabet = new HashSet<Character>();
		for(char c:string.toCharArray()){
			if(Character.isLetter(c) && !currentAlphabet.contains(c)){
				currentAlphabet.add(c);
			}
		}
		if(currentAlphabet.size() == 26){
			return true;
		}
		return false;
	}

	/**
	 * 17. Calculate the moment when someone has lived for 10^9 seconds.
	 * 
	 * A gigasecond is 109 (1,000,000,000) seconds.
	 * 
	 * @param given
	 * @return
	 */
	public Temporal getGigasecondDate(Temporal given) {
		// TODO Write an implementation for this method declaration
		int oldYear;
		int oldMonth;
		int oldDay;
		int oldHour;
		int oldMunite;
		int oldSecond;
		try {
			oldYear = given.get(ChronoField.YEAR);
			oldMonth = given.get(ChronoField.MONTH_OF_YEAR);
			oldDay = given.get(ChronoField.DAY_OF_MONTH);
			oldHour = given.get(ChronoField.HOUR_OF_DAY);
			oldMunite = given.get(ChronoField.MINUTE_OF_HOUR);
			oldSecond = given.get(ChronoField.SECOND_OF_MINUTE);
		} catch (Exception e){
			oldYear = given.get(ChronoField.YEAR);
			oldMonth = given.get(ChronoField.MONTH_OF_YEAR);
			oldDay = given.get(ChronoField.DAY_OF_MONTH);
			oldHour = 0;
			oldMunite = 0;
			oldSecond = 0;
		}
		LocalDateTime now = LocalDateTime.of(oldYear, oldMonth , oldDay, oldHour, oldMunite, oldSecond);
		LocalDateTime future = now.plusSeconds((long) Math.pow(10,9));
		return future;

//		int year = 31;
//		int month = 9;
//		int day = 8;
//		int hour = 1;
//		int miniute = 46;
//		int second = 40;
//
//		int oldYear = given.get(ChronoField.YEAR);
//		int oldMonth = given.get(ChronoField.MONTH_OF_YEAR);
//		int oldDay = given.get(ChronoField.DAY_OF_MONTH);
//		int oldHour = given.get(ChronoField.HOUR_OF_DAY);
//		int oldMunite = given.get(ChronoField.MINUTE_OF_HOUR);
//		int oldSecond = given.get(ChronoField.SECOND_OF_MINUTE);
//
//		int newYear;
//		int newMonth;
//		int newDay ;
//		int newHour;
//		int newMunite;
//		int newSecond;
//		newSecond = oldSecond + second;
//		if(newSecond >= 60){
//			newMunite = oldMunite + miniute + 1;
//			newSecond -= 60;
//		}else{
//			newMunite = oldMunite + miniute;
//		}
//		if(newMunite >= 60){
//			newHour = oldHour + hour + 1;
//			newMunite -= 60;
//		}else{
//			newHour = oldHour + hour;
//		}
//		if(newHour >= 24){
//			newDay = oldDay + day + 1;
//			newHour -= 24;
//		}else{
//			newDay = oldDay + day
//		}

	}

	/**
	 * 18. Given a number, find the sum of all the unique multiples of particular
	 * numbers up to but not including that number.
	 * 
	 * If we list all the natural numbers below 20 that are multiples of 3 or 5, we
	 * get 3, 5, 6, 9, 10, 12, 15, and 18.
	 * 
	 * The sum of these multiples is 78.
	 * 
	 * @param i
	 * @param set
	 * @return
	 */
	public int getSumOfMultiples(int i, int[] set) {
		// TODO Write an implementation for this method declaration
		int number = i;
		int sum = 0;
		for(int j=1; j<number; j++){
			for(int m: set){
				if(j % m ==0){
					sum += j;
					break;
				}
			}
		}
		return sum;
	}

	/**
	 * 19. Given a number determine whether or not it is valid per the Luhn formula.
	 * 
	 * The Luhn algorithm is a simple checksum formula used to validate a variety of
	 * identification numbers, such as credit card numbers and Canadian Social
	 * Insurance Numbers.
	 * 
	 * The task is to check if a given string is valid.
	 * 
	 * Validating a Number Strings of length 1 or less are not valid. Spaces are
	 * allowed in the input, but they should be stripped before checking. All other
	 * non-digit characters are disallowed.
	 * 
	 * Example 1: valid credit card number 1 4539 1488 0343 6467 The first step of
	 * the Luhn algorithm is to double every second digit, starting from the right.
	 * We will be doubling
	 * 
	 * 4_3_ 1_8_ 0_4_ 6_6_ If doubling the number results in a number greater than 9
	 * then subtract 9 from the product. The results of our doubling:
	 * 
	 * 8569 2478 0383 3437 Then sum all of the digits:
	 * 
	 * 8+5+6+9+2+4+7+8+0+3+8+3+3+4+3+7 = 80 If the sum is evenly divisible by 10,
	 * then the number is valid. This number is valid!
	 * 
	 * Example 2: invalid credit card number 1 8273 1232 7352 0569 Double the second
	 * digits, starting from the right
	 * 
	 * 7253 2262 5312 0539 Sum the digits
	 * 
	 * 7+2+5+3+2+2+6+2+5+3+1+2+0+5+3+9 = 57 57 is not evenly divisible by 10, so
	 * this number is not valid.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isLuhnValid(String string) {
		// TODO Write an implementation for this method declaration
		string = string.replaceAll(" ","");
		int sum = 0;
		int number;
		for (int i = 0; i < string.length(); i++){
			char c = string.charAt(string.length() - i - 1);
			if(!Character.isDigit(c)){
				return false;
			}
			if(i % 2 == 0){ //first, third....
				number = Character.getNumericValue(c);
				sum += number;
			}else{ //second...
				number = 2 * Character.getNumericValue(c);
				if(number > 9){
					number = number - 9;
				}
				sum += number;
			}
		}
		if(sum % 10 == 0){
			return true;
		}
		return false;
	}

	/**
	 * 20. Parse and evaluate simple math word problems returning the answer as an
	 * integer.
	 * 
	 * Add two numbers together.
	 * 
	 * What is 5 plus 13?
	 * 
	 * 18
	 * 
	 * Now, perform the other three operations.
	 * 
	 * What is 7 minus 5?
	 * 
	 * 2
	 * 
	 * What is 6 multiplied by 4?
	 * 
	 * 24
	 * 
	 * What is 25 divided by 5?
	 * 
	 * 5
	 * 
	 * @param string
	 * @return
	 */
	public int solveWordProblem(String string) {
		// TODO Write an implementation for this method declaration
		String[] words = string.substring(0,string.length()-1).split(" ");
		//ArrayList<Double> numbers = new ArrayList<Double>();
		int firstNum = Integer.parseInt(words[2]);
		int secondNum = Integer.parseInt(words[words.length-1]);
		int result = 0;
		String operation = words[3];
		switch(operation){
			case "plus":
				result = firstNum + secondNum;
				break;
			case "minus":
				result = firstNum - secondNum;
				break;
			case "multiplied":
				result = firstNum * secondNum;
				break;
			case "divided":
				result = firstNum / secondNum;
				break;
			default:
				throw new IllegalArgumentException();
		}
		return result;
	}

}
